# mount (legacy) cgroup controllers

mountpoint -q /sys/fs/cgroup || return 0
test -z "${container:-}" || return 0

for cgrp in $(awk '$4 == 1 { print $1 }' /proc/cgroups); do
	_dir="/sys/fs/cgroup/${cgrp}"
	mountpoint -q "${_dir}" && continue
	test -d "${_dir}" || mkdir -p "${_dir}"
	mount -t cgroup -o "${cgrp}" cgroup "${_dir}"
done
