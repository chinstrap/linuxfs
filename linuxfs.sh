#!/bin/sh

set -eu

linuxfs_dir="$(dirname "$(readlink -f "$0")")"

test ! -e "${linuxfs_dir}/apifs.fstab" || {
	mount -anT "${linuxfs_dir}/apifs.fstab" 2>/dev/null || :
	test -e /dev/shm || ln -sf /run/shm /dev/shm
	# Lock down permission in /proc and /sys
	chmod -f 0700 -- /sys/kernel/debug || :
	chmod -f 0400 -- /proc/slabinfo || :
}

test ! -e "${linuxfs_dir}/container-detect.sh" || {
	. "${linuxfs_dir}/container-detect.sh"
	if test "${container:-}"; then
		printf '%s\n' "${container:-}" > /run/container_type
	else
		rm -f -- /run/container_type
	fi
}

test ! -e "${linuxfs_dir}/cgroup.sh" || . "${linuxfs_dir}/cgroup.sh"

test -z "$@" || exec $@
