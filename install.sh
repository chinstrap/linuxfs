#!/bin/sh

data="
 apifs.fstab
 container-detect.sh
 cgroup.sh"

scripts="linuxfs.sh"

set -eux

install -Dm 0644 -t "$1" $data
install -Dm 0755 -t "$1" $scripts
