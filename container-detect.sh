test -z "${container:-}" || return 0

if test "${LIBVIRT_LXC_UUID:-}"; then
	container=lxc-libvirt
elif test -d /proc/vz && test ! -d /proc/bc; then
	container=openvz
else
	VXID="$(cat /proc/self/status | grep ^VxID | cut -f2)" || :
	test "${VXID:-0}" -le 1 || container=vserver
fi

test -z "${container:-}" || export container
